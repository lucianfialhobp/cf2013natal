$(function($){

    $("#formNatal").submit(function(){
        
        event.preventDefault();
        
        //Pegando os valores dos campos
        var cpf         =   $("#cpfUser").val();
        var nomeFull    =   $("#nameFull").val();
        var cep         =   $("#cepUser").val();
        var endereco    =   $("#addressUser").val();
        var numero      =   $("#numberUser").val();
        var complemento =   $("#complementUser").val();
        var cidade      =   $("#cityUser").val();
        var estado      =   $("#stateUser").val();
        var email       =   $("#emailUser").val();
        var telefone    =   $("#phoneUser").val();
        var codigoPromo =   $("#codeParticipate").val();

        /* 
        //Vendo se os campos estão vazios
        if (cpf ==='' || nomeFull ==='' || cep ==='' || endereco ==='' || numero ==='' || complemento ==='' || cidade ==='' || estado ==='' || email ==='' || telefone ==='' || codigoPromo ==='') {
           alert("Campos vazios");
           return;
        }
        //Validando CPF
        var countCpf = cpf.length;
 
        if (countCpf === 14 || countCpf === 11){
            cpfR = cpf.replace(/\D/g, '');            
            if (cpfR == "00000000000" || 
                cpfR == "11111111111" || 
                cpfR == "22222222222" || 
                cpfR == "33333333333" || 
                cpfR == "44444444444" || 
                cpfR == "55555555555" || 
                cpfR == "66666666666" || 
                cpfR == "77777777777" || 
                cpfR == "88888888888" || 
                cpfR == "99999999999"
                ) {
                alert("Número de CPF inválido!");
            }
            var a = [];
            var b = new Number;
            var c = 11;
        
            for (i=0; i<11; i++) {
                a[i] = cpfR.charAt(i);
                if (i < 9) b += (a[i] * --c);
                }
                if ((x = b % 11) < 2){
                a[9] = 0
                }else {
                    a[9] = 11-x
                }

                b = 0;
                c = 11;

                for (y=0; y<10; y++) b += (a[y] * c--);
                    if ((x = b % 11) < 2) {
                        a[10] = 0;
                    }else {
                        a[10] = 11-x;
                }

                if ((cpfR.charAt(9) != a[9]) || (cpfR.charAt(10) != a[10])) {
                    alert("Número de CPF inválido!");
                    return;
                }
        }else {
            alert("Quantidade de caracteres inválido"); 
            return;
        }
        */
        $.ajax({
            cache: false,
            dataType: "jsonp",
            url:  "http://localhost:8080/ld-natal-cf-disney/form.php",
            data: {             
                // cpf             : cpf,
                // nomeCompleto    : nomeFull,
                // cep             : cep,
                // endereco        : endereco,
                // numero          : numero,
                // complemento     : complemento,
                // cidade          : cidade,
                // estado          : estado,
                // email           : email,
                // telefone        : telefone,
                // codigoPromo     : codigoPromo 

                // Dados fakes para testes
                cpf         : "13793736709",
                nomeCompleto: "Cecilia Fialho",
                cep         : "25520200",
                endereco    : "Rua dona Julia Cheuen",
                numero      : "123",
                complemento : "456",
                cidade      : "SJM",
                estado      : "RJ",
                email       : "lucianfialhobp@hotmail.com",
                telefone    : "27569878",
                codigoPromo : "123"
            },
            success: function(response){
                console.log(response);
                console.log("Sucesso");
            }, error: function(response){
                console.log(response);
                console.log("error");
            }
        });
    });
});