<?php

// Previnindo SQLinject
// Tranformando meu GET em uma váriavel separada
$dados = $_GET;

// Verificando se o servidor tem a opcao magic quotes gpc ativado se sim slashe no array
if (get_magic_quotes_gpc()){
    $dados = array_map("stripcslashes", $dados);
}

// Mapeando o array 
$dados = array_map("mysql_real_escape_string", $dados);

// Variaveis tratadas
$cpf = $dados['cpf'];
$nomeCompleto = $dados['nomeCompleto'];
$cep = $dados['cep'];
$endereco = $dados['endereco'];
$numero = $dados['numero'];
$complemento = $dados['complemento'];
$cidade = $dados['cidade'];
$estado = $dados['estado'];
$email = $dados['email'];
$telefone = $dados['telefone'];
$codigoPromo = $dados['codigoPromo'];

// Iniciando Conexão com o banco de dados
$conexao = mysql_connect('localhost', 'root', '');

if (!$conexao) {
    die("Error".mysql_error());
}

$banco = mysql_select_db("natalcf", $conexao);
mysql_set_charset ('UTF8', $conexao);

if (!$banco) {
    die("Error select bd".mysql_error());
}
// Com certeza essa função deve ficar separada desse arquivo
function validarCPF( $cpfV = '' ) { 

  $cpfV = str_pad(preg_replace('/[^0-9]/', '', $cpfV), 11, '0', STR_PAD_LEFT);

    // Verifica se nenhuma das sequências abaixo foi digitada, caso seja, retorna falso
    if ( strlen($cpfV) != 11 || $cpfV == '00000000000' || $cpfV == '11111111111' || $cpfV == '22222222222' || $cpfV == '33333333333' || $cpfV == '44444444444' || $cpfV == '55555555555' || $cpfV == '66666666666' || $cpfV == '77777777777' || $cpfV == '88888888888' || $cpfV == '99999999999') {
        return false;
    } else { // Calcula os números para verificar se o CPF é verdadeiro
        for ($t = 9; $t < 11; $t++) {
            for ($d = 0, $c = 0; $c < $t; $c++) {
                $d += $cpfV{$c} * (($t + 1) - $c);
            }
            $d = ((10 * $d) % 11) % 10;
            if ($cpfV{$c} != $d) {
                return false;
            }
        }
        return $cpfV;
    }
}
$cpfValid = validarCPF($cpf);

// Armazenando em variavel CPF sem caractere especiais
if ($cpfValid === false) {
    echo  $_GET['callback'] . '(' . json_encode(array(
            'cpf' => false,
            'mensagem'  => htmlspecialchars('CPF invalido pro backend')
        )) . ')';
    die();
}else {
    $cpf = $cpfValid;
}

$addUser = "INSERT INTO `usuarios` (`cpf`, `nome_completo`, `cep` ,`endereco`, `numero`, `complemento`, `cidade`, `estado`, `email`, `telefone`, `id`) VALUES ('$cpf','$nomeCompleto','$cep','$endereco','$numero','$complemento','$cidade','$estado','$email','$telefone', '')";

$validaCpfExistente = mysql_query ("SELECT * FROM usuarios WHERE cpf = '$cpf'");

// Existe um jeito melhor de se fazer essa verificação mas não to com mto tempo
if (@mysql_num_rows($validaCpfExistente) > 0){
    echo  $_GET['callback'] . '(' . json_encode(array(
        'usuario_existente' => true,
        'mensagem'  => htmlspecialchars('Cpf ja cadastrado em nossa base')
    )) . ')';
    die();
}else {
    // Adiciona usuario no banco
    $addBanco = mysql_query($addUser, $conexao);

    // Validando código promocional
    // Pegando current ID    
    $id_usuario = mysql_insert_id();

    // Verifica se o código é valido
    $CodeValidate = mysql_query("SELECT * FROM codigos WHERE codigo = '$codigoPromo'");

    $rowCodeValidate = mysql_fetch_array($CodeValidate);

    if ($rowCodeValidate['codigo'] === null) {
        echo "morri porque o codigo ta errado";
        die();
    }else {
        // Verifica se ja alguem vinculado ao codigo digitado pelo usuario
        // Se o campo id_usuario === null entao vincula o id do usuario ao codigo promocional
        $row = mysql_fetch_array($CodeValidate);
        echo $id_usuario;
        if ($row['id_usuario'] === null) {
            $addCode = "UPDATE codigos SET id_usuario = '$id_usuario' WHERE codigo = '$codigoPromo'";
            $addUserCode = mysql_query($addCode, $conexao);
            if (!$addUserCode) {
                die("Error select bd".mysql_error());
            }
        }
    }

    // Verifica se o usuário foi realmente adicionado no banco
    if (!$addBanco) {
        die("Error select bd".mysql_error());
    }else {
        echo  $_GET['callback'] . '(' . json_encode(array(
            'cadastrado' => true,
            'mensagem'  => htmlspecialchars('Usuario cadastrado com sucesso')
        )) . ')';
    }
}
exit();